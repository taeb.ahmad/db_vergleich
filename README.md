# DB_Vergleich



## Getting started

One time installing

pip install -r requirements.txt

python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver

After changes to the database structure (models.py)

python manage.py makemigrations
python manage.py migrate
python manage.py runserver