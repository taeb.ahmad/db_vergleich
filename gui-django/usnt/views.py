import json
import os
import traceback
from sqlite3 import OperationalError

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseServerError, Http404
from django.views.decorators.csrf import csrf_exempt
from fuzzywuzzy import fuzz

from usnt.compare import DbComparer
from usnt.utils import path_leaf, readable_size
import time


class JSON:
    def dumps(obj):
        return json.dumps(obj, default=lambda o: o.__dict__)


class Table:
    def __init__(self, headers: list[str], rows: list['Row'], table_name: str):
        self.headers = headers
        self.rows = rows
        self.table_name = table_name


class Row:
    def __init__(self, values: list[str]):
        self.values = values


def jqp_fuzz_ratio(request):
    value_left = request.POST.get('value_left')
    value_right = request.POST.get('value_right')
    res = fuzz.ratio(str(value_left), str(value_right))
    context = {'res': res}
    return JsonResponse(context)


def jqp_update_db(request):
    new_value = request.POST.get('new_value')
    db_side = request.POST.get('db_side')
    table_name = request.POST.get('table_name')
    row_id = request.POST.get('row_id')
    column_name = request.POST.get('column_name')

    # dbComparer = DbComparer()
    with dbComparer:
        side = None
        if db_side == 'Left':
            side = dbComparer.left
        elif db_side == 'Right':
            side = dbComparer.right
        else:
            return HttpResponseServerError(f"Error: db_side:{db_side} not known")
        crsr = side.cursor()
        crsr.execute(f"SELECT type FROM pragma_table_info(?) WHERE name = ?", [table_name, column_name]);
        columntype = crsr.fetchone()[0];
        #crsr.close();
        #crsr = side.cursor();
        try:
            if columntype == 'REAL':
                float(new_value)
            elif columntype == 'INTEGER':
                int(new_value)
            elif columntype == 'TEXT':
                pass
            elif columntype.startswith('varchar'):
                pass
            else:
                return HttpResponseServerError(f"Error: can not change content of columntype: {columntype}")
        except ValueError as ex:
            msg = str(ex)
            return HttpResponseServerError('Data validation => ValueError: ' + msg)

        try:
            crsr.execute(f"UPDATE [{table_name}] SET [{column_name}] = ? WHERE rowid = ?;", [new_value, row_id]);

            if crsr.rowcount == 1:
                side.conn.commit()
        except OperationalError as ex:
            traceback.print_exc()
            msg = str(ex)
            if msg == 'database is locked':
                msg += ', this error happens if you are changing data with the App DB Browser and there is unsaved Data'
            return HttpResponseServerError('updating => OperationalError: ' + msg)
        context = {'rowcount': crsr.rowcount}
        return JsonResponse(context)

def jqp_example_differents(request):
    # dbComparer = DbComparer()
    with dbComparer:
        crsr_left1 = dbComparer.left.cursor()
        crsr_right1 = dbComparer.right.cursor()



        #return HttpResponseServerError("Error: not same Schema: " + "<br /><br />" + "<br />".join(differents))

        context = {'res': 'test_' + 'table_name' + '_' + 'row_id'}
        return JsonResponse(context)


def jqp_get_differents(request):
    # dbComparer = DbComparer()
    with dbComparer:
        try:
            dbComparer.compare()
        except OperationalError as ex:
            traceback.print_exc()
            return HttpResponseServerError('comparing => OperationalError: ' + str(ex))

        #return HttpResponseServerError("Error: not same Schema: " + "<br /><br />" + "<br />".join(differents))

        context = {
            'diff': json.loads(JSON.dumps(dbComparer.pullDataDifferences())),
            'unvisited': dbComparer.pull_unvisited()
        }
        return JsonResponse(context)


def jqp_cell_clicked(request):
    table_name = request.POST.get('table_name')
    row_id = request.POST.get('row_id')
    # todo: delete this test function
    context = {'res': 'test_' + table_name + '_' + row_id}
    return JsonResponse(context)


dbComparer = DbComparer()

def jqp_get_tables(request):
    leftDb = request.POST.get('leftDb')
    rightDb = request.POST.get('rightDb')

    if leftDb is None:
        return HttpResponseServerError('opening left database => leftDb is None: ')
    if rightDb is None:
        return HttpResponseServerError('opening right database => rightDb is None: ')

    leftDb = path_leaf(leftDb)
    rightDb = path_leaf(rightDb)
    upload_folder = DbComparer().specification_upload_folder

    # dbComparer = DbComparer()
    dbComparer.left.str_DB = os.path.join(upload_folder, leftDb)
    dbComparer.right.str_DB = os.path.join(upload_folder, rightDb)
    with dbComparer:
        crsr_left1 = None
        crsr_right1 = None
        try:
            crsr_left1 = dbComparer.left.cursor()
        except OperationalError as ex:
            traceback.print_exc()
            return HttpResponseServerError('opening left database => OperationalError: ' + str(ex))

        try:
            crsr_right1 = dbComparer.right.cursor()
        except OperationalError as ex:
            traceback.print_exc()
            return HttpResponseServerError('opening right database => OperationalError: ' + str(ex))

        crsr_left_tables = dbComparer.left.cursor()
        crsr_left_tables.execute(dbComparer.specification_SQL_getAllTables)

        db_tablesL = []
        db_tablesR = []
        for table_info in crsr_left_tables:
            str_fromTable = table_info[0]  # eine der abgerufenen Tabellen ist in der Variable "fromTable"

            crsr_left1.execute(f'SELECT rowid, * FROM {str_fromTable}')
            crsr_right1.execute(f'SELECT rowid, * FROM {str_fromTable}')

            differents = dbComparer.compareSchema();
            if differents is not None:
                return HttpResponseServerError("Error: not same Schema: " + "<br /><br />" + "<br />".join(differents))

            rowsL = Row(crsr_left1.fetchall())
            rowsR = Row(crsr_right1.fetchall())

            columnnamesL = list(map(lambda x: x[0], crsr_left1.description))
                            # DB1 : Objekt, das alle informationen beenhaltet, die verglichen werden sollen.
            columnnamesR = list(map(lambda x: x[0], crsr_right1.description))
                            # DB2 : Objekt, das alle informationen beenhaltet, die verglichen werden sollen.
            tableL = Table(columnnamesL, rowsL, str_fromTable)
            tableR = Table(columnnamesR, rowsR, str_fromTable)

            db_tablesL.append(json.loads(JSON.dumps(tableL)))
            db_tablesR.append(json.loads(JSON.dumps(tableR)))

        context = {
            'db_tablesL': db_tablesL,
            'db_tablesR': db_tablesR,
        }
        return JsonResponse(context);


def compare(request):
    if request.POST:
        jqp_function = request.POST.get('jqp_function')
        if jqp_function is not None and str(jqp_function).startswith('jqp_'):
            jqp_func = globals()[str(jqp_function)]
            return jqp_func(request)

    return render(request, 'interface.html', {})


def start(request):
    if request.POST:
        jqp_function = request.POST.get('jqp_function')
        if jqp_function is not None and str(jqp_function).startswith('jqp_'):
            jqp_func = globals()[str(jqp_function)]
            return jqp_func(request)

    upload_folder = DbComparer().specification_upload_folder
    try:
        if not os.path.exists(upload_folder):
            os.makedirs(upload_folder)
    except FileExistsError as ex:
        traceback.print_exc()
        msg = str(ex)
        return HttpResponseServerError('error while creating upload Folder: ' + msg)
    uploadedFile = 'null'
    if request.FILES:
        for filename, file in request.FILES.items():
            file_name = request.FILES[filename].name
            file_path = os.path.join(upload_folder, file_name);
            path = default_storage.save(file_path, ContentFile(file.read()))
            uploadedFile = path_leaf(path)
            # dbComparer.left.str_DB = path
            # print(path)
            # file.save(os.path.join(upload_folder, filename))

    filelist_rows = []
    filelist_header = ['Dateiname', 'Größe', 'Änderungszeit']
    files = os.listdir(upload_folder)
    for filename in files:
        file_path = os.path.join(upload_folder, filename)
        if os.path.isfile(file_path):
            bytes = os.path.getsize(file_path)

            # ctime1 = os.path.getctime(file_path)
            # ctime2 = time.ctime(ctime1)

            mtime1 = os.path.getmtime(file_path)
            mtime2 = time.ctime(mtime1)
            filelist_rows.append((filename, readable_size(bytes), mtime2))

    context = {'filelist_header': filelist_header, 'filelist_rows': filelist_rows, 'uploadedFile': uploadedFile}
    return render(request, 'start.html', context)


def jqp_delete_file(request):
    path = request.POST.get('path')
    filename = path_leaf(path)
    upload_folder = DbComparer().specification_upload_folder
    file_path = os.path.join(upload_folder, filename)
    if os.path.exists(file_path):
        os.remove(file_path)
        context = {'res': 'done'}
        return JsonResponse(context)
    else:
        return Http404


def download(request, path):
    upload_folder = DbComparer().specification_upload_folder
    filename = path_leaf(path)
    file_path = os.path.join(upload_folder, filename)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as file_stream:
            response = HttpResponse(file_stream.read(), content_type="application/vnd.sqlite3")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404
