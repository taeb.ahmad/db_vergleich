import sqlite3

import numpy
from joblib.numpy_pickle_utils import xrange
from fuzzywuzzy import fuzz, process
from numpy.f2py.crackfortran import _is_visit_pair

from usnt.tableColumns import Db
import json


class DbSide:
    def __init__(self):
        self.str_DB = None
        self.conn = None
        self.__all_crsrs = []
    def cursor(self):
        if self.conn is None:
            #self.conn = sqlite3.connect(self.str_DB)
            self.conn = sqlite3.connect('file:' + self.str_DB + '?mode=rw', uri=True)

        crsr = self.conn.cursor();
        self.__all_crsrs.append(crsr)
        return crsr

    def close(self):
        for crsr in self.__all_crsrs:
            crsr.close()
        self.__all_crsrs = []
        if self.conn is not None:
            self.conn.close()
            self.conn = None


class DbComparer:
    def __init__(self):
        self.left = DbSide()
        self.right = DbSide()
        self.__stringBuilder = []
        self.__dataDifferences = []
        self.__visitedIdsLeft = {}
        self.__visitedIdsRight = {}

        # Alle Tabellen, die mit "rpl" Präfix abrufen
        self.specification_SQL_getAllTables = 'SELECT tbl_name FROM sqlite_master WHERE type = "table" AND tbl_name LIKE "rpl_%"'

        self.specification_srcTable = 'rpl_paper'
        self.specification_srcColumn = 'title'
        self.specification_upload_folder = 'Upload' #  todo app.config['UPLOAD_FOLDER']


        # todo let user choose two DB to compare
        self.left.str_DB = r'C:\Users\Ahmad\Desktop\BachelorArbeit\Datenbanken-zum-Vergleich-2\2022-07-18_Teil2_innewformat.sqlite3'
        self.right.str_DB = r'C:\Users\Ahmad\Desktop\BachelorArbeit\Datenbanken-zum-Vergleich-2\2022-07-18_Teil2_innewformat_wchanges.sqlite3'
        #self.right.str_DB = r'C:\Users\Ahmad\Desktop\BachelorArbeit\echte_DB\2023-01-31_AMO_db_for_paper.sqlite3'

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close();

    def close(self):
        self.left.close()
        self.right.close()

    def compareTableSchema(self, str_fromTable):
        same_schema = True

        dbComparer = self
        crsr_left1 = dbComparer.left.cursor()
        crsr_right1 = dbComparer.right.cursor()

        sqlQuery = f"SELECT name, type FROM pragma_table_info('{str_fromTable}') " #todo + " ORDER BY name"

        crsr_left1.execute(sqlQuery)
        crsr_right1.execute(sqlQuery)

        table1 = crsr_left1.fetchall()
        table2 = crsr_right1.fetchall()

        i1 = len(table1) - 1
        while i1 >= 0:
            found = False
            i2 = len(table2) - 1
            while i2 >= 0:
                if table1[i1][0] == table2[i2][0]:
                    found = True
                    if table1[i1][1] != table2[i2][1]:
                        same_schema = False
                        self.__stringBuilder.append(f'Schema difference in table: {str_fromTable}, types: {table1[i1][1]} != {table1[i2][1]} in columnname: {table1[i1][0]}')
                    else:
                        table2.pop(i2)
                    break
                i2 = i2 - 1

            if not found:
                same_schema = False
                self.__stringBuilder.append(f'Schema difference in table: {str_fromTable}, columnname: {table1[i1][0]} not found in the right DB')
            i1 = i1 - 1

        if len(table2) > 0:
            same_schema = False
            self.__stringBuilder.append(f'Schema difference in table: {str_fromTable}. the following columns were found only in the right db')
            self.__stringBuilder.append(str(table2))
            self.__stringBuilder.append('')

        return same_schema

    def compareSchema(self):
        dbComparer = self
        crsr_left1 = dbComparer.left.cursor()

        crsr_left1.execute(
            'SELECT tbl_name FROM sqlite_schema WHERE type = "table" AND tbl_name LIKE "rpl_%"')  # Alle Tabellen, die mit "rpl" Präfix abrufen
        same_schema = True
        self.__stringBuilder = []

        for table_info in crsr_left1:
            str_fromTable = table_info[0]  # eine der abgerufenen Tabellen ist in der Variable "fromTable"
            same_schema = dbComparer.compareTableSchema(str_fromTable) and same_schema

        if same_schema:
            return None
        else:
            res = self.__stringBuilder
            self.__stringBuilder = []
            return res

    def build_foreign_key_list(self):
        dbComparer = self
        crsr_left1 = dbComparer.left.cursor()

        crsr_left_tables = dbComparer.left.cursor()
        crsr_left_tables.execute(dbComparer.specification_SQL_getAllTables)

        db = Db()

        for table_info in crsr_left_tables:
            str_fromTable = table_info[0]  # eine der abgerufenen Tabellen ist in der Variable "fromTable"

            crsr_left1.execute(
                'SELECT * FROM pragma_foreign_key_list("' + str_fromTable + '")')  # Informationen über Fremdschlüsselbeziehungen in der aktuellen Tabelle abrufen

            for row1 in crsr_left1:
                columnnames = list(map(lambda x: x[0], crsr_left1.description))
                #    columnnames['id', 'seq', 'table', 'from', 'to', 'on_update', 'on_delete', 'match']

                str_toTable = row1[columnnames.index('table')]
                str_toClmn = row1[columnnames.index('to')]
                str_fromClmn = row1[columnnames.index('from')]
                clmnTo = db.add(str_toTable).add(str_toClmn)
                clmnFrom = db.add(str_fromTable).add(str_fromClmn)
                clmnFrom.pragma_foreign_key_to = clmnTo
                clmnTo.add_pragma_foreign_key_from(str_fromTable, str_fromClmn, clmnFrom)

        return db

    def compare(self):
        db = self.build_foreign_key_list()
        self.compare_record(db, self.specification_srcTable)

    def compare_record(self, db, current_table, id1=None, id2=None, src=None, visited=None, table_path=None,
                     left_id_path=None, right_id_path=None):  # Datensatzvergleich
        if right_id_path is None:
            right_id_path = []
        if left_id_path is None:
            left_id_path = []
        if table_path is None:
            table_path = []
        if visited is None:
            visited = {}

        if current_table in table_path:
            return
        else:
            table_path = table_path.copy()
            table_path.append(current_table)
        query = 'SELECT rowid, * FROM ' + current_table
        crsr1 = self.left.cursor()
        crsr2 = self.right.cursor()
        idColumnname = 'rowid'
        if id1 is not None:
            crsr1.execute(query + f' WHERE {idColumnname} = {str(id1)}')
        else:
            crsr1.execute(query)
        # Inhalt einer Tabelle durchlaufen und jeden Datensatz im anderen Datenbank finden
        # Iterieren über Datensätze in der linken Datenbank
        for row1 in crsr1:
            tableRowId = (current_table, row1[0])
            if tableRowId in visited:
                continue
            visited[tableRowId] = True
            columnnames = list(map(lambda x: x[0], crsr1.description))  # Spaltennamen der aktuellen Tabelle
            if id1 is None:
                # Suchen nach einem Datensatz mit demselben Titel in der rechten Datenbank
                src = row1[columnnames.index(self.specification_srcColumn)]
                if src is None:
                    diff = DataDifference()
                    diff.TablePath = table_path
                    diff.LeftIdPath = left_id_path + [row1[0]]
                    #diff.RightIdPath = right_id_path + [row2[0]]
                    diff.Columns = [self.specification_srcColumn]
                    diff.Text = f"Warning: ignoring NULL in left {self.specification_srcTable}.{self.specification_srcColumn}"
                    self.__dataDifferences.append(diff)
                    self.__store_visited(self.__visitedIdsLeft, current_table, row1[0])
                    continue
                crsr2.execute(query + f' WHERE {self.specification_srcColumn} = ?', [src])
            else:
                crsr2.execute(query + f' WHERE {idColumnname} = {str(id2)}')

            found1 = False
            for row2 in crsr2:
                self.__store_visited(self.__visitedIdsLeft, current_table, row1[0])
                self.__store_visited(self.__visitedIdsRight, current_table, row2[0])
                if not found1:
                    found1 = True
                else:
                    diff = DataDifference()
                    diff.TablePath = table_path
                    diff.LeftIdPath = left_id_path + [row1[0]]
                    diff.RightIdPath = right_id_path + [row2[0]]
                    diff.Columns = [self.specification_srcColumn]
                    diff.Text = f'Warning: {self.specification_srcTable}.{self.specification_srcColumn} must be unique, more than one row found for: {src}'
                    self.__dataDifferences.append(diff)
                for i in xrange(1, len(columnnames)):  # ignore 0 because of select rowid, * ...
                    columninfo = db.get(current_table, columnnames[i])
                    if columnnames[i] in ['is_help_needed']:
                        continue
                    elif columninfo is not None and columninfo.pragma_foreign_key_to is not None:
                        # print(f'db.get({current_table}, {columnnames[i]}).pragma_foreign_key_to: {row1[i]}<>{row2[i]} ' + str(columninfo.pragma_foreign_key_to))
                        if columninfo.pragma_foreign_key_to.table.tableName != self.specification_srcTable:
                            if row1[i] is None and row2[i] is None:
                                continue
                            elif row1[i] is None or row2[i] is None:
                                diff = DataDifference()
                                diff.TablePath = table_path
                                diff.LeftIdPath = left_id_path + [row1[0]]
                                diff.RightIdPath = right_id_path + [row2[0]]
                                diff.Columns = [columnnames[i]]
                                diff.Text = f'one table has reference, other table has NULL {row1[i]}!={row2[i]} in {current_table}.{columninfo.column_name}  --> {src}'
                                self.__dataDifferences.append(diff)
                            else:
                                self.compare_record(db, columninfo.pragma_foreign_key_to.table.tableName, row1[i], row2[i], src,
                                             visited, table_path, left_id_path + [row1[0]], right_id_path + [row2[0]])
                        pass
                    elif columninfo is not None and len(columninfo.pragma_foreign_key_froms) > 0:
                        # print(f'db.get({current_table}, {columnnames[i]}).pragma_foreign_key_froms: ' + str(len(columninfo.pragma_foreign_key_froms)))
                        for frm in columninfo.pragma_foreign_key_froms.values():
                            # Multivergleich
                            fromAndJoinPart = f' FROM {frm.table.tableName} '
                            clmns = frm.table.columns.values()
                            joinTablenames = [frm.table.tableName]
                            asIndex = 0
                            for clmn in clmns:
                                if clmn == frm:
                                    continue
                                # join
                                if clmn.pragma_foreign_key_to is not None:
                                    tableName = clmn.pragma_foreign_key_to.table.tableName
                                    if tableName in joinTablenames:
                                        asIndex += 1
                                        fromAndJoinPart += 'LEFT OUTER JOIN ' + tableName + ' AS ' + tableName + str(
                                            asIndex) + ' ON ' + str(
                                            clmn) + '=' + clmn.pragma_foreign_key_to.table.tableName + str(
                                            asIndex) + '.' + clmn.pragma_foreign_key_to.column_name + ' \n'
                                        joinTablenames.append(tableName + str(asIndex))
                                    else:
                                        fromAndJoinPart += 'LEFT OUTER JOIN ' + tableName + ' ON ' + str(
                                            clmn) + '=' + str(clmn.pragma_foreign_key_to) + ' \n'
                                        joinTablenames.append(tableName)

                            selectPartWithSeparator = ''
                            selectPartWithoutSeparator = ''
                            for s in joinTablenames:
                                if selectPartWithSeparator != '':
                                    selectPartWithSeparator += ', '
                                    selectPartWithoutSeparator += ', '
                                else:
                                    selectPartWithSeparator += f'";", {s}.rowid, {s}.*'
                                    selectPartWithoutSeparator += f'{s}.rowid, {s}.*'
                                    continue
                                selectPartWithSeparator += f'";", {s}.*'
                                selectPartWithoutSeparator += f'{s}.*'

                            frmCrsr1 = self.left.cursor()
                            frmCrsr2 = self.right.cursor()

                            frmCrsr1.execute('SELECT ' + selectPartWithSeparator + fromAndJoinPart + f' WHERE FALSE')
                            # frmCrsr1.fetchall()
                            frmColumnnames = []
                            tableIndex = -1
                            for c in frmCrsr1.description:
                                clmnname = c[0]
                                if clmnname == '";"':
                                    tableIndex += 1
                                else:
                                    frmColumnnames.append((joinTablenames[tableIndex], clmnname))

                            frmQuery = 'SELECT ' + selectPartWithoutSeparator + fromAndJoinPart + f' WHERE {frm} = '

                            frmCrsr1.execute(frmQuery + str(row1[0]))
                            frmCrsr2.execute(frmQuery + str(row2[0]))

                            frmList1 = frmCrsr1.fetchall()
                            frmList2 = frmCrsr2.fetchall()

                            frmColumnnames2 = list(map(lambda x: x[0], frmCrsr2.description))
                            if len(frmColumnnames) != len(frmColumnnames2):
                                # todo show assertion like maxPos Warning
                                print(frmColumnnames)
                                print(frmColumnnames2)
                                print([clmn for (tbl, clmn) in frmColumnnames])
                                print(
                                    f'{len(frmColumnnames)} != {len(frmColumnnames2)} length of results are not same :( ')

                            similarity = numpy.zeros(  # Ähnlichkeitsmatrix
                                (len(frmList1), len(frmList2)))  # [[0]*len(frmList2)] * len(frmList1)
                            for index1_r in xrange(len(frmList1)):
                                for index2_r in xrange(len(frmList2)):
                                    for index_clmn in xrange(len(frmColumnnames)):
                                        if db.get(frmColumnnames[index_clmn][0],
                                                  frmColumnnames[index_clmn][1]) is not None:
                                            if frmList1[index1_r][index_clmn] is None or frmList2[index2_r][index_clmn] is None:
                                                continue
                                            similarity[index1_r][index2_r] += fuzz.ratio(
                                                str(frmList1[index1_r][index_clmn]),
                                                str(frmList2[index2_r][index_clmn])) # todo use columnname instead of index

                            # Multi-Vergleich-Zuordnung anhand der Ähnlichkeitsmatrix (similarity):
                            # max finden
                            mapRows = []
                            while len(mapRows) < len(frmList1) and len(mapRows) < len(frmList2):
                                max = -1
                                maxPos = None
                                for index1_r in xrange(len(frmList1)):
                                    # debugvar1 = [x for (x, y) in mapRows if x == index1_r]
                                    if index1_r in [x for (x, y) in mapRows if x == index1_r]:
                                        continue
                                    for index2_r in xrange(len(frmList2)):
                                        # debugvar1 = [y for (x, y) in mapRows if y == index2_r]
                                        if index2_r in [y for (x, y) in mapRows if y == index2_r]:
                                            continue
                                        if max < similarity[index1_r][index2_r]:
                                            max = similarity[index1_r][index2_r]
                                            maxPos = (index1_r, index2_r)

                                if maxPos is None:
                                    print('Warning maxPos is None!!')
                                    self.__stringBuilder.append('Warning maxPos is None!!')
                                    diff = DataDifference()
                                    diff.TablePath = table_path
                                    diff.LeftIdPath = left_id_path + [row1[0]]
                                    diff.RightIdPath = right_id_path + [row2[0]]
                                    diff.Columns = [columnnames[i]]
                                    diff.Text = f'assertion warning maxPos is None!!'
                                    self.__dataDifferences.append(diff)
                                else:
                                    mapRows.append(maxPos)

                            # rows only in one side
                            for index1_r in xrange(len(frmList1)):
                                # debugvar1 = [x for (x, y) in mapRows if x == index1_r]
                                if index1_r in [x for (x, y) in mapRows if x == index1_r]:
                                    pass
                                else:
                                    stringBuilder = []
                                    stringBuilder.append(f'the foreign key {frm} to {current_table} has not the same count of referencing rows: {len(frmList1)}:{len(frmList2)} ')
                                    stringBuilder.append(f'the following referencing row does not have a compare-partner in the right table')
                                    stringBuilder.append(strFrmColumnnames(frmColumnnames))
                                    stringBuilder.append(strDatarow(frmColumnnames, frmList1, index1_r))
                                    stringBuilder.append(f' --> src:{src} ')
                                    diff = DataDifference()
                                    diff.TablePath = table_path + [[frm.table.tableName]]
                                    diff.LeftIdPath = left_id_path + [row1[0], frmList1[index1_r][0]]
                                    diff.RightIdPath = right_id_path + [row2[0]]
                                    diff.Columns = [frm.column_name]
                                    diff.Text = "<BR />".join(str(e) for e in stringBuilder)
                                    self.__dataDifferences.append(diff)
                            for index2_r in xrange(len(frmList2)):
                                # debugvar1 = [y for (x, y) in mapRows if y == index2_r]
                                if index2_r in [y for (x, y) in mapRows if y == index2_r]:
                                    pass
                                else:
                                    stringBuilder = []
                                    stringBuilder.append(f'the foreign key {frm} to {current_table} has not the same count of referencing rows: {len(frmList1)}:{len(frmList2)} ')
                                    stringBuilder.append(f'the following referencing row does not have a compare-partner in the left table ')
                                    stringBuilder.append(strFrmColumnnames(frmColumnnames))
                                    stringBuilder.append(strDatarow(frmColumnnames, frmList2, index2_r))
                                    stringBuilder.append(f' --> src:{src} ')
                                    diff = DataDifference()
                                    diff.TablePath = table_path + [[frm.table.tableName]]
                                    diff.LeftIdPath = left_id_path + [row1[0]]
                                    diff.RightIdPath = right_id_path + [row2[0] , frmList2[index2_r][0]]
                                    diff.Columns = [frm.column_name]
                                    diff.Text = "<BR />".join(str(e) for e in stringBuilder)
                                    self.__dataDifferences.append(diff)

                            left_id_path2 = left_id_path + [row1[0]]
                            right_id_path2 = right_id_path + [row2[0]]
                            for m in mapRows:
                                self.compare_record(db, frm.table.tableName, frmList1[m[0]][0], frmList2[m[1]][0], src, visited,
                                             table_path, left_id_path2, right_id_path2)
                        pass
                    elif row1[i] != row2[i]:
                        if columnnames[i] == 'id':
                            continue
                        stringBuilder = []
                        stringBuilder.append(f'Nichtübereinstimmung gefunden ')
                        stringBuilder.append(f'{row1[i]} != {row2[i]}')
                        stringBuilder.append(f'in {current_table}.{columnnames[i]}')
                        stringBuilder.append(f'{row1}   (left  database)')
                        stringBuilder.append(f'{row2}   (right database)')
                        stringBuilder.append(f' --> src:{src} ')
                        diff = DataDifference()
                        diff.TablePath = table_path
                        diff.LeftIdPath = left_id_path + [row1[0]]
                        diff.RightIdPath = right_id_path + [row2[0]]
                        diff.Columns = [columnnames[i]]
                        diff.Text = "<BR />".join(str(e) for e in stringBuilder)
                        self.__dataDifferences.append(diff)
                    # else: print('ok')
            if not found1:
                diff = DataDifference()
                diff.TablePath = table_path
                diff.LeftIdPath = left_id_path + [row1[0]]
                diff.RightIdPath = right_id_path  # + [row2[0]]
                diff.Columns = [self.specification_srcColumn]
                diff.Text = f'Table: {current_table}, row not found in the left DB:   {row1}'
                self.__dataDifferences.append(diff)

    def pullDataDifferences(self):
        res = self.__dataDifferences;
        self.__dataDifferences = [];
        return res;

    def __store_visited(self, visited_ids, table, row_id):
        if table not in visited_ids:
            visited_ids[table] = {row_id}
        else:
            visited_ids[table].add(row_id)

    def pull_unvisited(self):
        left = self.__get_unvisited2(self.__visitedIdsLeft, self.left.cursor())
        right = self.__get_unvisited2(self.__visitedIdsRight, self.right.cursor())
        self.__visitedIdsLeft = {}
        self.__visitedIdsRight = {}
        return {'left': left, 'right': right}

    def __get_unvisited2(self, visited_ids, crsr):
        res = []
        for table in visited_ids:
            str_ids = ", ".join(str(x) for x in visited_ids[table])
            res_unvisited_ids = []
            crsr.execute(f'SELECT rowid from {table} WHERE rowid not in ({str_ids})')
            for row in crsr:
                res_unvisited_ids.append(row[0])
            if len(res_unvisited_ids) > 0:
                res.append({'table': table, 'ids': res_unvisited_ids})
        return res

def strFrmColumnnames(frmColumnnames):
    stringBuilder = []
    printedTable = None
    for (table, column) in frmColumnnames:
        if printedTable != table:
            if printedTable is not None:
                stringBuilder.pop()
                stringBuilder.append(') JOIN ')
            printedTable = table
            stringBuilder.append(printedTable + '(')
        stringBuilder.append(column)
        stringBuilder.append(', ')
    if printedTable is not None:
        stringBuilder.pop()
        stringBuilder.append(') ')
    return "".join(stringBuilder)

def strDatarow(frmColumnnames, frmList, index_r):
    stringBuilder = []
    printedTable = None
    tmp = frmColumnnames[0][0]
    for f in xrange(len(frmColumnnames)):
        table = frmColumnnames[f][0]
        if printedTable != table:
            if printedTable is not None:
                stringBuilder.pop()
                stringBuilder.append(') JOIN ')
            printedTable = table
            stringBuilder.append(printedTable + '(')
        stringBuilder.append(frmList[index_r][f])
        stringBuilder.append(', ')
    if printedTable is not None:
        stringBuilder.pop()
        stringBuilder.append(') ')
    return "".join(str(e) for e in stringBuilder)


class DataDifference:
    def __init__(self):
        self.Text = None
        self.Columns = []

        self.TablePath = []
        self.LeftIdPath = []
        self.RightIdPath = []



