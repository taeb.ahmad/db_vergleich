class Db:

    def __init__(self):
        self.tables = {}

    def add(self, tableName):
        if tableName not in self.tables:
            self.tables[tableName] = DbTable(self, tableName)
        return self.tables[tableName]

    def get(self, tableName, columnName=None):
        if tableName not in self.tables:
            return None
        if columnName is None:
            return self.tables[tableName]
        return self.tables[tableName].get(columnName)


class DbTable:

    def __init__(self, db, tableName):
        self.tableName = tableName
        self.db = db
        self.columns = {}

    # def print_table(self):
    #     for column in self.columns:
    #         column.print_columns()

    def add(self, column_name):
            if column_name not in self.columns:
                self.columns[column_name] = DbColumn(self, column_name)
            return self.columns[column_name]

    def get(self, column_name):
        if column_name in self.columns:
            return self.columns[column_name]
        else:
            return None

class DbColumn:

    def __init__(self, table, column_name):
        self.table=table
        self.column_name = column_name
        self.daten_type = None # todo
        self.pragma_foreign_key_to = None
        self.pragma_foreign_key_froms = {}

    def __str__(self):
        return self.table.tableName + '.' + self.column_name

    def add_pragma_foreign_key_from(self, fromTable, fromColumn, value):
        from2 = (fromTable, fromColumn)
        if from2 not in self.pragma_foreign_key_froms:
            self.pragma_foreign_key_froms[from2] = value
        return self.pragma_foreign_key_froms[from2]

    def get_pragma_foreign_key_from(self, fromTable, fromColumn):
        from2 = (fromTable, fromColumn)
        if from2 not in self.pragma_foreign_key_froms:
            return None
        return self.pragma_foreign_key_froms[from2]







